require_relative 'models'

class Exoboard < Sinatra::Base
  get '/' do

    @winners = Winner.all.sort_by!{|w| -w.tweets.size}

    haml :index
  end
end
