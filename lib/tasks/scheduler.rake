desc "This task updates the database every hour"
task :update_db => :environment do
  puts "Updating db..."
  new_records = Processor.update_db
  puts "done. #{new_records} new tweet(s) processed."
end
