require 'andand'
require 'twitter'
require_relative 'models'

Twitter.configure do |config|
  config.consumer_key = ENV['CONSUMER_KEY']
  config.consumer_secret = ENV['CONSUMER_SECRET']
  config.oauth_token = ENV['OAUTH_TOKEN']
  config.oauth_token_secret = ENV['OAUTH_SECRET']
end

class Processor
  def self.last_tweet_id
    Tweet.first(:order => [ :tweet_id.desc ]).andand.tweet_id
  end

  def self.first_tweet_id
    Tweet.first(:order => [ :tweet_id.asc ]).andand.tweet_id
  end

  def self.save_tweet(tweet)
    of_sep = ' of '
    comma_sep = ', '
    of_index = tweet.text.rindex(of_sep)
    comma_index = tweet.text.rindex(comma_sep)
    return if of_index.nil? || comma_index.nil?

    winner_name = tweet.text.slice(0...comma_index).strip
    winner = Winner.first_or_create(name: winner_name)

    dino_name = tweet.text.slice(comma_index+comma_sep.length...of_index).strip
    dino = Dinosaur.first_or_create(name: dino_name)

    planet_name = tweet.text.slice(of_index+of_sep.length..-1).strip
    planet = Exoplanet.first_or_create(name: planet_name)

    tweet_id = tweet.id
    timestamp = tweet.created_at
    tweet = Tweet.first_or_create(tweet_id: tweet_id, timestamp: timestamp)
    dino.tweets << tweet
    winner.tweets << tweet
    planet.tweets << tweet
    dino.save
    winner.save
    planet.save
    tweet.save
  end

  # returns number of tweets added
  def self.update_db
    start_count = Tweet.count

    opts = { since: last_tweet_id, count: 200 }
    if last_tweet_id.nil?
      opts[:max_id] = first_tweet_id || (1 << (1.size * 8 - 2) - 1)
    end

    #this is the worst but the first tweet doesn't fit the format
    #and I'm too lazy to do this the right way soooooo
    while opts[:max_id] != 384407253719461888
      tweets = Twitter.user_timeline('exosaurs', opts)

      tweets.each do |tweet|
        save_tweet(tweet)
      end

      opts[:max_id] = first_tweet_id
      opts[:since] = nil

      break if tweets.size == 0
    end

    Tweet.count - start_count
  end
end
