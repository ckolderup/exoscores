require 'rubygems'
require 'bundler'

Bundler.require

require './processor'
require './exoboard'

run Exoboard
