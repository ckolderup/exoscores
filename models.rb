require 'dm-core'
require 'dm-migrations'

class Dinosaur
  include DataMapper::Resource
  property :id, Serial
  property :name, String, :length => 140
  has n, :tweets, :through => Resource
end

class Winner
  include DataMapper::Resource
  property :id, Serial
  property :name, String, :length => 140
  has n, :tweets, :through => Resource
end

class Exoplanet
  include DataMapper::Resource
  property :id, Serial
  property :name, String, :length => 140
  has n, :tweets, :through => Resource
end

class Tweet
  include DataMapper::Resource
  property :id, Serial
  property :tweet_id, Integer
  property :timestamp, DateTime
  has n, :dinosaurs, :through => Resource
  has n, :winners, :through => Resource
  has n, :exoplanets, :through => Resource
end

DataMapper.setup(:default, ENV['DATABASE_URL'] || "sqlite3://#{Dir.pwd}/database.db")
DataMapper.finalize
DataMapper.auto_upgrade!
